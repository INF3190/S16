/**
 * Application demo  @UQAM
 * node s16.js
 */
var express = require('express');
var app=express();
var bodyParser = require('body-parser');
var info=require('./backenddemo.js');
app.use(express.static(__dirname));
app.get('/', function(req,res){
	res.send("Allo? Le site fonctionne!");
});
app.get('/infocours', function(req,res){
	console.log(info.infocours());
	res.json(info.infocours());
});
var portNumber=3001;// au besoin changer le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');