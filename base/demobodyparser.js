/**
 * Application demo1  @UQAM
 * node s16.js
 */
var express = require('express');
var app=express();
var bodyParser = require('body-parser');
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.text({ type: 'text/html' }));
app.get('/', function(req,res){
	res.send("Allo? methode get");
});
//create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.post('/inscription',urlencodedParser, function(req,res){
	 res.json(req.body);
	//res.send("Allo? methode get bonjour");
});
app.post('/', function(req,res){
	res.send("Allo? methode post");
});
app.post("/u[a-z]{2}$|ets|poly", function(req,res){
	res.send("Allo? methode post dans une université");
});
app.put('/', function(req,res){
	res.send("Allo? méthode put");
});
app.all('/uqam', function(req,res){
	//pour toute methode avec chemin /uqam
	res.send("Allo? Vous êtes à l'UQAM");
});
var portNumber=3006;// au besoin changer le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');