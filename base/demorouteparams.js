/**
 * Application demo  @UQAM
 * node s16.js
 */
var express = require('express');
var app=express();

app.use(express.static(__dirname));
app.get('/', function(req,res){
	res.send("Allo? Le site fonctionne!");
});
app.get('/uqam/:codeperm/:prenom', function(req,res){
	var rep="Bonjour "+ req.params.prenom;
	rep= rep+" votre code permanent est ";
	rep= rep +req.params.codeperm;
	res.send(rep);
});
// tester sur un navigateur avec url:
// http://localhost:3003/uqam/ABCD12342178/Mike

var portNumber=3003;// au besoin changer le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');